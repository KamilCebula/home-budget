<?php

    session_start();
    include("connection.php");

    $missingEmail = '<p><stong>Please enter your email address!</strong></p>';
    $missingPassword = '<p><stong>Please enter your password!</strong></p>';
    $errors = ""; 

    // Check login email
    if (empty($_POST["login-username"])) {
        $errors .= $missingEmail;   
    } else {
        $username = filter_var($_POST["login-username"], FILTER_SANITIZE_EMAIL);
    }
    
    // Check login password
    if (empty($_POST["login-password"])) {
        $errors .= $missingPassword;   
    } else {
        $password = filter_var($_POST["login-password"], FILTER_SANITIZE_STRING);
    }

    // If there are any errors
    if ($errors) {
        $resultMessage = '<div class="alert alert-danger">' . $errors .'</div>';
        echo $resultMessage;   
    } else {
        // No errors
        $username = mysqli_real_escape_string($link, $username);
        $password = mysqli_real_escape_string($link, $password);
        $password = hash('sha256', $password);

        // Check useremail and password
        $sql = "SELECT * FROM users WHERE Username='$username' AND Password='$password' AND activation='activated'";
        $result = mysqli_query($link, $sql);
        
        if (!$result) {
            echo '<div class="alert alert-danger">Error running the query!</div>';
            exit;
        }
         
        // Dont match username and password
        $count = mysqli_num_rows($result);
        if ($count !== 1) {
            echo '<div class="alert alert-danger">Wrong Username or Password!</div>';
        } else {
            // Log the user in
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $_SESSION['user_id'] = $row['user_id'];
            $_SESSION['FirstName'] = $row['First_Name'];
            $_SESSION['LastName'] = $row['Last_Name'];
            $_SESSION['Username'] = $row['Username'];
            $_SESSION['Email'] = $row['Email'];
            echo "success";
        }
    }    
