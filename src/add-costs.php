<?php
    session_start();
    include("connection.php");

    // Errors
    $missingData = '<p><strong>Please select a data!</strong></p>';
    $missingName = '<p><strong>Please enter a name!</strong></p>';
    $missingAmount = '<p><strong>Please enter a amount!</strong></p>';
    $errors = "";

    // Required data:
    if (empty($_POST["add-costs-data"])) {
        $errors .= $missingData;
    } else {
        $data = filter_var($_POST["add-costs-data"], FILTER_SANITIZE_STRING);
    }

    // Required data:
    if (empty($_POST["add-costs-name"])) {
        $errors .= $missingName;
    } else {
        $name = filter_var($_POST["add-costs-name"], FILTER_SANITIZE_STRING);
    }

    // Required amount:
    if (empty($_POST["add-costs-amount"])) {
        $errors .= $missingAmount;
    } else {
        $amount = filter_var($_POST["add-costs-amount"], FILTER_SANITIZE_STRING);
    }

    if ($errors) {
        $resultMessage = '<div class="alert alert-danger">' . $errors . '</div>';
        echo $resultMessage;
        exit;
    }

    // Not required
    $category = $_POST["add-costs-category"];

    $person = $_POST["add-costs-person"];

    if (empty($_POST["add-costs-comment"])) {
        $comment = $_POST["add-costs-comment"];
    } else {
        $comment = filter_var($_POST["add-costs-amount"], FILTER_SANITIZE_STRING);
    }

    // All correct 
    $user_id = $_SESSION['user_id'];
    $data = mysqli_real_escape_string($link, $data);
    $name = mysqli_real_escape_string($link, $name);
    $amount = mysqli_real_escape_string($link, $amount);
    $category = mysqli_real_escape_string($link, $category);
    $person = mysqli_real_escape_string($link, $person);
    $comment = mysqli_real_escape_string($link, $comment);

    $sql = " INSERT INTO costs (`User_id`, `Data`, `Name`, `Category`, `Amount`, `Person`, `Comment`) VALUES ('$user_id', '$data', '$name', '$category', '$amount', '$person', '$comment') ";
    $result = mysqli_query($link, $sql);
    if (!$result) {
        echo '<div class="alert alert-danger">There was an error inserting the users details in the database!</div>'; 
        exit;
    } else {
        echo '<div class="alert alert-success">Added costs successfully!</div>'; 
    }

    

  