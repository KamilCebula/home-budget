<?php
    include("connection.php");
    session_start();

    $user_id = $_SESSION['user_id'];
    $missingName = '<p><strong>Please enter a person name!</strong></p>';
    $errors = "";

    // Data
    $data = filter_var($_POST["add-person-data"], FILTER_SANITIZE_STRING);

    // Name
    if (empty($_POST["add-person-name"])) {
        $errors .= $missingName;
    } else {
        $name = filter_var($_POST["add-person-name"], FILTER_SANITIZE_STRING);
    }

    // Dispaly errors
    if ($errors) {
        $resultMessage = '<div class="alert alert-danger">' . $errors . '</div>';
        echo $resultMessage;
        exit;
    }

    $name = mysqli_real_escape_string($link, $name);
    $sql = " SELECT Person FROM Person WHERE Person='$name'";
    $result = mysqli_query($link, $sql);
         
    // Check category
    $count = mysqli_num_rows($result);
    if ($count > 0) {
        echo '<div class="alert alert-danger">There is already such a person!</div>';
    } else {
        $sql = " INSERT INTO person (`User_id`, `Person`, `Data`) VALUES ('$user_id', '$name', '$data') ";
        $result = mysqli_query($link, $sql);
        if (!$result) {
            echo '<div class="alert alert-danger">There was an error inserting the users details in the database!</div>'; 
            exit;
        } else {
            echo '<div class="alert alert-success">Added costs successfully!</div>'; 
        }
    }