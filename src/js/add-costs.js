// Data input
Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});

$('.data').val(new Date().toDateInputValue());

// Submit add costs
$("#add-costs-form").submit(function(event) {
    event.preventDefault();
    const dataToPost = $(this).serializeArray();
    $("#add-costs-loader").show();
    $("#add-costs-button").hide();
    

    $.ajax ({
        url: "add-costs.php",
        type: "POST",
        data: dataToPost,
        success: function(data) {
            if(data) {
                console.log(dataToPost);
                $("#add-costs-message").html(data);
                $("#add-costs-loader").hide();
                $("#add-costs-button").show();
            }
        },
        error: function() {
            $("#add-costs-message").html("<div class='alert alert-danger'>There was an error the Ajax Call. Please try again later</div>");
            $("#add-costs-loader").hide();
            $("#add-costs-button").show();
        }
    })
})

// Submit add category
$("#add-category-form").submit(function(event) {
    event.preventDefault();
    const dataToPost = $(this).serializeArray();
    $("#add-category-loader").show();
    $("#add-category-button").hide();
    

    $.ajax ({
        url: "add-category.php",
        type: "POST",
        data: dataToPost,
        success: function(data) {
            if(data) {
                $("#add-category-message").html(data);
                $("#add-category-loader").hide();
                $("#add-category-button").show();
            }
        },
        error: function() {
            $("#add-category-message").html("<div class='alert alert-danger'>There was an error the Ajax Call. Please try again later</div>");
            $("#add-category-loader").hide();
            $("#add-category-button").show();
        }
    })
})

// Submit add person
$("#add-person-form").submit(function(event) {
    event.preventDefault();
    const dataToPost = $(this).serializeArray();
    $("#add-person-loader").show();
    $("#add-person-button").hide();

    $.ajax ({
        url: "add-person.php",
        type: "POST",
        data: dataToPost,
        success: function(data) {
            if(data) {
                location.reload('add-category.php');
                $("#add-person-message").html(data);
                $("#add-person-loader").hide();
                $("#add-person-button").show();
                $('#add-category-box').reset();
            }
        },
        error: function() {
            $("#add-person-message").html("<div class='alert alert-danger'>There was an error the Ajax Call. Please try again later</div>");
            $("#add-person-loader").hide();
            $("#add-person-button").show();
        }
    })
})
