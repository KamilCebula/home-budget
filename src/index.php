<?php
    session_start();

    include('connection.php');

    include('logout.php');

    // include('remember.php');
?>

<!DOCTYPE html>
<html lang="en">
<head> 
    <title>Home Budget</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Style -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#">Home Budget</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="list-item"><a href="#">Home</a></li>
                    <li class="list-item"><a href="#">Help</a></li>
                    <li class="list-item"><a href="#" >Contact us</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" data-toggle="modal" data-target="#siginUp-modal"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#login-modal"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Main -->
    <main class="main">
        <!-- Jumbotron -->
        <div class="jumbotron">
            <h1>Home Budget</h1>
            <p>Control your budget online</p>
            <button class="jumbotron__button" type="button" data-toggle="modal" data-target="#siginUp-modal">Sigin up - It's free</button>
        </div>

        <!-- Sigin in modal -->
        <div class="modal fade" id="siginUp-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Sigin up today and Start using our Home Budget App</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div> 
                    <!-- Modal body -->
                    <div class="modal-body">
                        <form method="post" id="sigin-up-form">
                            <div id="sigin-up-message"></div>
                            <div class="form-group">
                                <label for="sigin-u-first-name">Fist name:</label>
                                <input type="text" class="form-control" id="sigin-up-first-name" placeholder="Enter your first name" name="sigin-up-first-name" maxlength="30">
                            </div>
                            <div class="form-group">
                                <label for="sigin-up-last-name">last name:</label>
                                <input type="text" class="form-control" id="sigin-up-last-name" placeholder="Enter your last name" name="sigin-up-last-name" maxlength="30">
                            </div>
                            <div class="form-group">
                                <label for="email">Username:</label>
                                <input type="text" class="form-control" id="sigin-up-username" placeholder="Enter username" name="sigin-up-username" maxlength="30">
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="sigin-up-email" placeholder="Enter email" name="sigin-up-email" maxlength="50">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control" id="sigin-up-password" placeholder="Enter password" name="sigin-up-password" maxlength="30">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Confirm password:</label>
                                <input type="password" class="form-control" id="sigin-up-password2" placeholder="Confirm password" name="sigin-up-password2" maxlength="30">
                            </div>
                            <button type="submit" class="btn btn-primary" id="sigin-in-button">Sigin up</button>
                            <div class="loader" id="sigin-in-loader"></div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Login modal -->
        <div class="modal fade" id="login-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Login:</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div> 
                        <!-- Modal body -->
                        <div class="modal-body">
                            <form method="post" id="login-form">
                                <!-- Errors -->
                                <div id="login-message"></div>
                                <div class="form-group">
                                    <label for="login-username">Username:</label>
                                    <input type="text" class="form-control" id="login-username" placeholder="Enter username" name="login-username">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" id="login-password" placeholder="Enter password" name="login-password">
                                </div>
                                <div class="form-group form-check">
                                    <a class="pull-right forgot-link" data-dismiss="modal" data-target="#forgotPassword-modal" data-toggle="modal" >Forgot Password ?</a>
                                </div>
                                <button type="submit" class="btn btn-primary" id="login-button">Login</button>
                                <div class="loader" id="login-loader"></div>
                            </form>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Forgot Password Modal -->
        <div class="modal fade" id="forgotPassword-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Forgot Password? Enter your email adress:</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div> 
                        <!-- Modal body -->
                        <div class="modal-body">
                            <form method="post" id="forgotPassword-form">
                                <div id="forgot-message"></div>
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control" id="forgot-email" placeholder="Enter username" name="forgot-email">
                                </div>
                                <button type="submit" class="btn btn-primary" id="forgot-button">Send password</button>
                                <div class="loader" id="forgot-loader"></div>
                            </form>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    <!-- Script -->
    <script src="js/index.js"></script>
    <script src="js/script.js"></script>
</body>
</html>
