<?php

    session_start();

    if (!isset($_SESSION['user_id'])) {
        header("location: index.php");
    }

    include('connection.php');
    // Category
    $user_id = $_SESSION['user_id'];
    $sqlCategory = "SELECT * FROM category WHERE user_id='$user_id'";
    $resultCategory = mysqli_query($link, $sqlCategory);

    // Person
    $sqlPerson = "SELECT * FROM person WHERE user_id='$user_id'";
    $resultPerson = mysqli_query($link, $sqlPerson);
?>
<!DOCTYPE html>
<html lang="en">
<head> 
    <title>Notes Online-My Notes</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Style -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#">Notes Online</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="list-item"><a href="profile.php">Profile</a></li>
                    <li class="list-item"><a href="#">Analysis</a></li>
                    <li class="list-item active"><a href="mynotes.php">My budget</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" data-toggle="modal" data-target="#siginUp-modal"><span class="glyphicon glyphicon-user"></span> Logged in as <b><?php echo ($_SESSION['FirstName']) ." ". ($_SESSION['LastName']) ?></b></a></li>
                    <li><a href="index.php?logout=1"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Main -->
    <main class="main">
       <div class="container container-notes">
           <div class="row">
               <div class="col-md-offset-3 col-md-6">
                    <div id="notes-message"></div>
                    <div class="container-buttons">
                       <button class="btn btn-info btn-lg" type="button" id="add-costs" data-toggle="modal" data-target="#add-costs-modal">Add costs</button>
                       <button class="btn btn-info btn-lg" type="button" id="add-category" data-toggle="modal" data-target="#add-category-modal">Add category</button>
                       <button class="btn btn-info btn-lg" type="button" id="add-person" data-toggle="modal" data-target="#add-person-modal">Add person</button>
                    </div>
               </div>
           </div>
       </div>
       <!-- Add costs modal -->
       <div class="modal fade" id="add-costs-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Add costs:</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div> 
                    <!-- Modal body -->
                    <div class="modal-body">
                        <form method="post" id="add-costs-form">
                            <div id="add-costs-message"></div>
                            <div class="form-group">
                                <label for="add-costs-data">*Data:</label>
                                <input type="date" class="form-control data" id="add-costs-data" placeholder="Enter your first name" name="add-costs-data" value="date">
                            </div>
                            <div class="form-group">
                                <label for="add-costs-name">*Name:</label>
                                <input type="text" class="form-control" id="add-costs-name" placeholder="Enter name" name="add-costs-name" maxlength="30">
                            </div>
                            <div class="form-group">
                                <label for="add-costs-category">Category:</label>
                                <select class="add-costs-category form-control" id="add-costs-category" name="add-costs-category">
                                    <option value=""></option>
                                    <?php
                                        while ($row = mysqli_fetch_array($resultCategory)) {
                                            echo "<option value='" . $row['Category'] . "'>" . $row['Category'] . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="add-costs-amount">*Amount in dollars:</label>
                                <input type="number" class="form-control" id="add-costs-amount" placeholder="Enter username" name="add-costs-amount" maxlength="30">
                            </div>
                            <div class="form-group">
                                <label for="add-costs-person">Person:</label>
                                <select class="add-costs-person form-control" id="add-costs-person" name="add-costs-person">
                                <?php
                                    while ($row = mysqli_fetch_array($resultPerson)) {
                                        echo "<option value='" . $row['Person'] . "'>" . $row['Person'] . "</option>";
                                    } 
                                    if ($row == 0) {
                                        echo "<option value=''></option>";
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="add-costs-comment">Comment:</label>
                                <textarea class="form-control" id="add-costs-comment" name="add-costs-comment" maxlength="300"></textarea>
                            </div>
                            <p> * - Required fields</p>
                            <button type="submit" class="btn btn-primary" id="add-costs-button">Add</button>
                            <div class="loader" id="add-costs-loader"></div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
       <!-- Add category modal -->
       <div class="modal fade" id="add-category-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Add category:</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div> 
                    <!-- Modal body -->
                    <div class="modal-body">
                        <form method="post" id="add-category-form">
                            <div id="add-category-message"></div>
                            <div class="form-group">
                                <input type="hidden" class="form-control data" id="add-category-data" placeholder="Enter your first name" name="add-category-data" value="date">
                            </div>
                            <div class="form-group">
                                <label for="add-category-name">Name category:</label>
                                <input type="text" class="form-control" id="add-category-name" placeholder="Enter name" name="add-category-name" maxlength="30">
                            </div>
                            <div class="form-group" id="add-category-box">
                                <label for="add-category-category">Category:</label>
                                    <?php
                                        $sqlCategory = "SELECT * FROM category WHERE user_id='$user_id'";
                                        $resultCategory = mysqli_query($link, $sqlCategory);
                                        while ($row = mysqli_fetch_array($resultCategory)) {
                                            echo "<div>" . $row['Category'] . "</div>";
                                        }
                                    ?>
                            </div>
                            <button type="submit" class="btn btn-primary" id="add-category-button">Add</button>
                            <div class="loader" id="add-category-loader"></div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

       <!-- Add person modal -->
       <div class="modal fade" id="add-person-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Add person:</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div> 
                    <!-- Modal body -->
                    <div class="modal-body">
                        <form method="post" id="add-person-form">
                            <div id="add-person-message"></div>
                            <div class="form-group">
                                <input type="hidden" class="form-control data" id="add-person-data" placeholder="Enter your first name" name="add-person-data" value="date">
                            </div>
                            <div class="form-group">
                                <label for="add-person-name">Name person:</label>
                                <input type="text" class="form-control" id="add-person-name" placeholder="Enter name" name="add-person-name" maxlength="30">
                            </div>
                            <div class="form-group">
                                <label for="add-person-person">Person:</label>
                                    <?php
                                        $sqlPerson = "SELECT * FROM person WHERE user_id='$user_id'";
                                        $resultPerson = mysqli_query($link, $sqlPerson);
                                        while ($row = mysqli_fetch_array($resultPerson)) {
                                            echo "<div>" . $row['Person'] . "</div>";
                                        }
                                    ?>
                            </div>
                            <button type="submit" class="btn btn-primary" id="add-category-button">Add</button>
                            <div class="loader" id="add-category-loader"></div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
     <!-- Jquery -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Jquery UI -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- Script -->
    <script src="js/script.js"></script>
    <!-- Ajax -->
    <script src="js/add-costs.js"></script>
</body>
</html>
